package ru.korkmasov.tsc.api;

import ru.korkmasov.tsc.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findTaskByProjectId(String id);

    Task bindTaskById(String taskId, String projectId);

    Task unbindTaskById(String id);

    void removeTaskFromProjectById(String id);

}
