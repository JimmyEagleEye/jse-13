package ru.korkmasov.tsc.api;

import ru.korkmasov.tsc.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
