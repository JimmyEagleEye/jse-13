package ru.korkmasov.tsc.service;

import ru.korkmasov.tsc.api.IProjectTaskService;
import ru.korkmasov.tsc.api.ITaskRepository;
import ru.korkmasov.tsc.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    public ProjectTaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findTaskByProjectId(String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.findAllTaskByProjectId(id);
    }

    @Override
    public Task bindTaskById(String taskId, String projectId) {
        if (taskId == null || taskId.isEmpty()) return null;
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.bindTaskToProjectById(taskId, projectId);
    }

    @Override
    public Task unbindTaskById(String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.unbindTaskById(id);
    }

    @Override
    public void removeTaskFromProjectById(String id) {
        if (id == null || id.isEmpty()) return;
        taskRepository.removeAllTaskByProjectId(id);
    }

}
