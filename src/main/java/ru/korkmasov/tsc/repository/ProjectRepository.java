package ru.korkmasov.tsc.repository;

import ru.korkmasov.tsc.api.IProjectRepository;
import ru.korkmasov.tsc.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> list = new ArrayList<>();

    @Override
    public List<Project> findAll() {
        return list;
    }

    @Override
    public Project findById(final String id){
        for (Project project:list){
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findByName(final String Name){
        for (Project project:list){
            if (Name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project findByIndex(final int index){
        return list.get(index);
    }

    @Override
    public void add(Project project) {
        list.add(project);
    }

    @Override
    public void remove(final Project project) {
        list.remove(project);
    }

    @Override
    public Project removeById(final String id){
        final Project project = findById(id);
        if (project == null) return null;
        list.remove(project);
        return project;
    }

    @Override
    public Project removeByName(final String Name){
        final Project project = findByName(Name);
        if (project == null) return null;
        list.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex (final int index){
        final Project project = findByIndex(index);
        if (project == null) return null;
        list.remove(project);
        return project;
    }

    @Override
    public void clear() {
        list.clear();
    }

}
